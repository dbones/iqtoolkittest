﻿namespace IqLinqTest.App
{
    using System.Collections.Generic;
    using System.Linq;
    using LinqProvider;

    class Program
    {
        static void Main(string[] args)
        {

            var people = new List<Person>();
            var dave = new Person { Id = 1, Name = "Dave", Country = "Uk" };
            var chan = new Person { Id = 2, Name = "Chan", Country = "Uk" };

            people.Add(dave);
            people.Add(chan);

            var name = people.Search()
                //.Select(x=>x.Name)
                .Where(x => x.Name == "Chan" || x.Id >= 1)
                //.Where(x => x.Name.Contains("asd"))
                //.First(x => x.Name.Contains("asd"));


            //.OrderBy(x => x.Name)
            //.Skip(1).Take(8)
            .Select(x => new { x.Name, x.Country })
            //.Where(x => x.Name.ToLower().Contains("asassdadsad"))
            //.Skip(2).Take(2)
            //.Select(x => x.Name)
            //.OrderByDescending(x => x)
            //.Select(x => x[0].ToString())
            //.Aggregate((current, next) => current + next)
            .ToList();
            //.Any(x=>x.Contains("c"));


            
        }
    }
}
