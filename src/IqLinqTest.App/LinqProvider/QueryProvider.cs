namespace IqLinqTest.App.LinqProvider
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using IndexLinq;
    using IndexQuery;
    using IQToolkit;

    public class QueryProvider<T> : QueryProvider
    {
        private readonly List<T> _collection;
        public QueryProvider(List<T> collection)
        {
            _collection = collection;
        }

        public override string GetQueryText(Expression expression)
        {
            return "";
        }

        public override object Execute(Expression expression)
        {
            //preprocess
            expression = PartialEvaluator.Eval(expression);
            
            
            var linqQuery = LinqVisitor.Eval(expression);
            var luceneQuery = LuceneVisitor.Eval(linqQuery);
            
            //TODO: look into injection and decoupling a bit
            //var strategy = new LuceneProcessingStrategy();
            //strategy.Find(luceneQuery, linqQuery.Paging, null);
            
            
            //just for now, prove we can rewrite the collection source
            linqQuery.ParentQuery.RewriteSource(_collection);
            var exp = linqQuery.ParentQuery.Expression;
            

            //run the expression!
            var result = Expression.Lambda(exp).Compile().DynamicInvoke();
            return result;
        }

        
    }
}