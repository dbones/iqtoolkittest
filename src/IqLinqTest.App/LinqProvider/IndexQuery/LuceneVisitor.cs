namespace IqLinqTest.App.LinqProvider.IndexQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using IndexLinq;
    using Lucene.Net.Search;


    public class LuceneVisitor
    {
        
        private readonly LuceneQuery _luceneQuery = new LuceneQuery();

        public static LuceneQuery Eval(LinqQuery linqQuery)
        {
            var visitor = new LuceneVisitor();
            visitor.Visit(linqQuery);
            return visitor.Query;
        }

        public LuceneQuery Query { get { return _luceneQuery; } }

        private LuceneVisitor() { }

        public void Visit(LinqQuery linqQuery)
        {
            VisitWhereExpression(linqQuery.WhereClauses);
            VisitOrderByExpression(linqQuery.Ordering);
        }


        private void VisitWhereExpression(IEnumerable<Expression> expressions)
        {
            //Query query = null;
            foreach (var expression in expressions)
            {
                var temp = LuceneTreeVisitor.Eval(expression);

                if (_luceneQuery.Query == null)
                {
                    _luceneQuery.Query = temp;
                }
                else
                {
                    var bq = new BooleanQuery();
                    bq.Add(_luceneQuery.Query, Occur.MUST);
                    bq.Add(temp, Occur.MUST);
                    _luceneQuery.Query = bq;
                }
            }
        }

        private void VisitOrderByExpression(IEnumerable<OrderBy> orderings)
        {
            //TODO
            throw new NotImplementedException();
        }

    }
}