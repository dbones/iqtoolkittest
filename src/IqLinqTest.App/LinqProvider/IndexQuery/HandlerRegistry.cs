namespace IqLinqTest.App.LinqProvider.IndexQuery
{
    using System;
    using System.Collections.Generic;
    using Handlers;

    public static class HandlerRegistry
    {
        public static readonly IDictionary<Type, List<IHandler>> Handlers = new Dictionary<Type, List<IHandler>>();

        static HandlerRegistry()
        {
            Register<AndOrVisitorHandler>();
            Register<EqualityHandler>();
            Register<LessAndGreaterThanHandler>();
            Register<StringContainsHandler>();
        }

        public static void Register<T>() where T : IHandler, new()
        {
            var handler = new T();

            List<IHandler> handlers = null;
            if (!Handlers.TryGetValue(handler.HandleTypeOf, out handlers))
            {
                handlers = new List<IHandler>();
                Handlers.Add(handler.HandleTypeOf, handlers);
            }

            handlers.Add(handler);
        }
    }
}