namespace IqLinqTest.App.LinqProvider.IndexQuery.Handlers
{
    using System.Linq.Expressions;
    using Lucene.Net.Index;
    using Lucene.Net.Search;

    public class EqualityHandler : BinaryHandler
    {
        public override void Handle(BinaryExpression expression, VisitorContext context)
        {
            var nameValue = GetNameValue(expression);

            var name = GetMemberName(nameValue.Member);
            var term = new Term(name, nameValue.Constant.Value == null ? "" : nameValue.Constant.Value.ToString());
            context.SetResult(new TermQuery(term));
        }

        public override bool CanHandle(BinaryExpression expression)
        {
            return expression.NodeType == ExpressionType.Equal;
        }

    }
}