namespace IqLinqTest.App.LinqProvider.IndexQuery.Handlers
{
    using System.Linq.Expressions;
    using Lucene.Net.Search;

    public class AndOrVisitorHandler : HandlerBase<BinaryExpression>
    {
        public override void Handle(BinaryExpression expression, VisitorContext context)
        {
            var isAnd = expression.NodeType == ExpressionType.AndAlso ||
                        expression.NodeType == ExpressionType.And;
            
            context.Visit(expression.Left);
            context.Visit(expression.Right);

            var right = context.GetResult();
            var left = context.GetResult();

            var query = new BooleanQuery();
            var occour = isAnd
                ? Occur.MUST
                : Occur.SHOULD;
            query.Add(new BooleanClause(left, occour));
            query.Add(new BooleanClause(right, occour));
            
            context.SetResult(query);
        }

        public override bool CanHandle(BinaryExpression expression)
        {
            var isAnd = expression.NodeType == ExpressionType.AndAlso ||
                        expression.NodeType == ExpressionType.And;
            var isOr = expression.NodeType == ExpressionType.OrElse ||
                       expression.NodeType == ExpressionType.Or;

            var supported = isAnd || isOr;

            return supported;
        }
    }
}