namespace IqLinqTest.App.LinqProvider.IndexQuery.Handlers
{
    using System.Linq.Expressions;
    using Lucene.Net.Search;

    public class LessAndGreaterThanHandler : BinaryHandler
    {
        public override void Handle(BinaryExpression expression, VisitorContext context)
        {
            var isLessThan = expression.NodeType == ExpressionType.LessThan;
            var isLessThanOrEqual = expression.NodeType == ExpressionType.LessThanOrEqual;
            var isGreaterThan = expression.NodeType == ExpressionType.GreaterThan;
            var isGreaterThanOrEqual = expression.NodeType == ExpressionType.GreaterThanOrEqual;

            var nameValue = GetNameValue(expression);
            var name = GetMemberName(nameValue.Member);

            string upper = isLessThan || isLessThanOrEqual ? nameValue.Constant.Value.ToString() : null;
            string lower = isGreaterThan || isGreaterThanOrEqual ? nameValue.Constant.Value.ToString() : null;

            var query = new TermRangeQuery(name, lower, upper, isGreaterThanOrEqual, isLessThanOrEqual);
            context.SetResult(query);
        }


        public override bool CanHandle(BinaryExpression expression)
        {
            var isLessThan = expression.NodeType == ExpressionType.LessThan;
            var isLessThanOrEqual = expression.NodeType == ExpressionType.LessThanOrEqual;
            var isGreaterThan = expression.NodeType == ExpressionType.GreaterThan;
            var isGreaterThanOrEqual = expression.NodeType == ExpressionType.GreaterThanOrEqual;

            var supported = isLessThan || isLessThanOrEqual || isGreaterThan || isGreaterThanOrEqual;

            return supported;
        }
    }
}