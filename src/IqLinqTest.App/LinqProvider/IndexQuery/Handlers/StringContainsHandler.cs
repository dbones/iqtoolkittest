namespace IqLinqTest.App.LinqProvider.IndexQuery.Handlers
{
    using System;
    using System.Linq.Expressions;
    using Lucene.Net.Index;
    using Lucene.Net.Search;

    public class StringContainsHandler : MethodHandler
    {
        public override void Handle(MethodCallExpression expression, VisitorContext context)
        {
            var cValue = expression.Arguments[0] as ConstantExpression;
            if (cValue == null)
            {
                throw new NotSupportedException("requires a parameter");
            }

            var name = GetMemberName((MemberExpression)expression.Object);
            var term = new Term(name, string.Format("{0}{1}{0}", "*", cValue.Value));
            var query = new WildcardQuery(term);
            context.SetResult(query);
        }

        public override bool CanHandle(MethodCallExpression expression)
        {
            bool isString = GetDeclaringType(expression) == typeof(string);
            bool isMethod = GetMethodName(expression).Equals("Contains");

            return (!(isString && isMethod));
        }
    }
}