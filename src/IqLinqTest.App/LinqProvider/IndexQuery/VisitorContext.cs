namespace IqLinqTest.App.LinqProvider.IndexQuery
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Lucene.Net.Search;

    public class VisitorContext
    {
        private readonly Stack<Query> _terms = new Stack<Query>(5);
        private readonly ExpressionVisitor _visitor;

        public VisitorContext(ExpressionVisitor visitor)
        {
            _visitor = visitor;
        }

        public void Visit(Expression expression)
        {
            _visitor.Visit(expression);
        }


        public Query GetResult()
        {
            return _terms.Pop();
        }

        public void SetResult(Query query)
        {
            _terms.Push(query);
        }

    }
}