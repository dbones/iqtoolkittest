namespace IqLinqTest.App.LinqProvider
{
    using System.Collections.Generic;
    using System.Linq;
    using IQToolkit;

    public static class SearchExtensions
    {
        public static IQueryable<T> Search<T>(this List<T> collection)
        {
            return new Query<T>(new QueryProvider<T>(collection));
        }
    }
}