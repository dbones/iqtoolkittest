namespace IqLinqTest.App.LinqProvider.IndexLinq
{
    public enum OrderByDirection
    {
        Asc,
        Desc
    }
}