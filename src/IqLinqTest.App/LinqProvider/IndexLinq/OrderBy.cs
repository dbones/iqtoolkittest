namespace IqLinqTest.App.LinqProvider.IndexLinq
{
    using System.Linq.Expressions;

    public class OrderBy
    {
        public Expression Expression { get; set; }
        public OrderByDirection Direction { get; set; }
    }
}