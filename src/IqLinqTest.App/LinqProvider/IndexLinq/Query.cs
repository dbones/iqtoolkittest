namespace IqLinqTest.App.LinqProvider.IndexLinq
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using IQToolkit;

    public class Query
    {
        public Query(MethodCallExpression expression)
        {
            Expression = expression;
            SourceType = null;
            DestinationType = null;
        }

        /// <summary>
        /// the type which this expression relies upon
        /// 
        /// IEnumerable[Person].Select(p => p.Name)
        /// 
        /// the source type is Person
        /// </summary>
        public Type SourceType { get; private set; }

        /// <summary>
        /// the type which this expression returns
        /// 
        /// IEnumerable[Person].Select(p => p.Name)
        /// 
        /// the source type is string (the Name property is a string)
        /// </summary>
        public Type DestinationType { get; private set; }

        public MethodCallExpression Expression { get; private set; }

        public void RewriteSource(IEnumerable newSource)
        {

            //var queryableType = typeof(Queryable);
            //var asQueryable = queryableType.GetMethods(BindingFlags.Static | BindingFlags.Public)
            //    .First(x => x.Name == "AsQueryable" && x.GetParameters().Count() == 1);

            //var queryable = asQueryable.Invoke(null, new object[] { newSource });
            var queryable = newSource.AsQueryable();

            var newSourceConstant = System.Linq.Expressions.Expression.Constant(queryable);


            //LambdaExpression lambda = (LambdaExpression)Expression.Arguments[1];

            //var compiled = lambda.Compile();

            


            var updated = this.Expression.Update(this.Expression.Object,
                new[] {newSourceConstant, this.Expression.Arguments[1]});

            Expression = updated;


        }
    }

    
}