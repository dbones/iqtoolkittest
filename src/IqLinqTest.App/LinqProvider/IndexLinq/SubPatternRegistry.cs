﻿namespace IqLinqTest.App.LinqProvider.IndexLinq
{
    using System.Collections.Generic;
    using Handlers;

    public static class SubPatternRegistry
    {
        public static List<ISubPatternHandler> Handlers  { get; set; } 

        static SubPatternRegistry()
        {
            Handlers = new List<ISubPatternHandler>
            {
                new AllSubPatternHandler(),
                new AnySubPatternHandler(),
                new FirstSubPatternHandler(),
                new OrderBySubPatternHandler(),
                new SkipSubPatternHandler(),
                new TakeSubPatternHandler(),
                new WhereSubPatternHandler()
            };
        }
    }
}