namespace IqLinqTest.App.LinqProvider
{
    public class Paging
    {
        public Paging()
        {
            //default page sizes
            Skip = 0;
            Take = 128; 
        }

        public long Skip { get; set; }
        public long Take { get; set; }

        public long NResults { get { return Skip + Take; } }
    }
}