namespace IqLinqTest.App.LuceneProcessing
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using LinqProvider;
    using Lucene.Net.Documents;
    using Lucene.Net.Search;
    using Lucene.Net.Store;

    public class LuceneProcessingStrategy
    { 
        public FullTextResultList<LuceneResult> Find(LuceneQuery lucene, Paging paging, IEnumerable<Identifier> identifiers)
        {
            var searcher = GetSearcher();
            var docs = Search(searcher, lucene, paging.NResults);
            var resultIds = LoadPagedIdField(searcher, docs.ScoreDocs, identifiers, paging);
            return resultIds;
        }


        protected virtual Searcher GetSearcher()
        {
            return new IndexSearcher(new SimpleFSDirectory(new DirectoryInfo("")));
        }

        protected virtual TopDocs Search(Searcher searcher, LuceneQuery lucene, long nResults)
        {
            var results = searcher.Search(lucene.Query, (int)nResults);
            return results;
        }

        protected virtual FullTextResultList<LuceneResult> LoadPagedIdField(Searcher searcher, ScoreDoc[] scoreDocs, IEnumerable<Identifier> identifiers, Paging paging)
        {
            var results = new FullTextResultList<LuceneResult>((int)paging.Take);

            var idents = identifiers.ToDictionary(x => x.TypeName);
            var fields = idents.Values.Select(x => x.IdName).Union(new[] { "_type" }).Distinct();

            var fieldSelector = new SetBasedFieldSelector(new HashSet<string>(fields), new HashSet<string>());
            
            for (var i = (paging.Skip); i < (paging.Skip + paging.Take); i++)
            {
                ScoreDoc scoreDoc = scoreDocs[i];
                Document doc = searcher.Doc(scoreDoc.Doc, fieldSelector);
                var result = GetResult(doc, idents);
                results.Add(result);
            }

            return results;
        }

        private static LuceneResult GetResult(Document doc, IDictionary<string, Identifier> identifiers)
        {
            var type = doc.GetField("_type").StringValue;
            var idName = identifiers[type].IdName;
            var id = doc.GetField(idName).StringValue;

            return new LuceneResult {Id = id, TypeName = type};
        }
    }
}