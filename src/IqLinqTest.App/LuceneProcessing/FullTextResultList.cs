namespace IqLinqTest.App.LuceneProcessing
{
    using System.Collections.Generic;

    public class FullTextResultList<T> : List<T>
    {
        public FullTextResultList(int capacity) : base(capacity)
        {
            
        }

        public long TotalResults { get; set; }
        
        public long Start { get; set; }
        public long Take { get; set; }
    }
}