namespace IqLinqTest.App.LuceneProcessing
{
    public class Identifier
    {
        public string IdName { get; set; }
        public string TypeName { get; set; }
    }
}