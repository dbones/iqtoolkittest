namespace IqLinqTest.App.LuceneProcessing
{
    public class LuceneResult
    {
        public string Id { get; set; }
        public string TypeName { get; set; }
    }
}