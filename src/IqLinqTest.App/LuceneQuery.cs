﻿namespace IqLinqTest.App
{
    using System.Collections.Generic;
    using LinqProvider;
    using Lucene.Net.Search;

    public class LuceneQuery
    {
        private readonly List<Sort> _sortBys;

        public LuceneQuery()
        {
            Paging = new Paging();
            _sortBys = new List<Sort>();
        }

        public IEnumerable<Sort> Sort { get { return _sortBys; } }

        public Paging Paging { get; set; }

        public Query Query { get; set; }

        /// <summary>
        /// add a sort to the query, note that the order they are added are the order they will be evaluated.
        /// </summary>
        public void AddSort(Sort sort)
        {
            _sortBys.Add(sort);
        }

    }

    

}