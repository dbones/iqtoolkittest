﻿#summary
this project is part of a larger project called "ink" which is to be in junction with projects such as "ArmChair"

this bit is about isolating a part of a linq statement which can easily be taken care of via a Lucene index

once isolated it is to be completely replaced with the executed results.

the rest of the query will be applied in proc

	var name = people.Search()
				//this bit will be search with by Lucene
                .Where(x => x.Name == "Chan" || x.Id >= 1)
                .Where(x => x.Name.Contains("asd"))
				.OrderBy(x => x.Name)
				.Skip(1)
				.Take(8)
				
				//as ArmChair is to work with CouchDb, projection will always happen in proc
				.Select(x => new { x.Name, x.Country })
				.Where(x => x.Name.ToLower().Contains("asassdadsad"))
				.ToList();

The above is a single LINQ statement, which will be processed partially by Lucene and partially in proc


#Notes

	while, all |                                  => while (take -1)
	orderby, while, while, orderby, take |        => orderdy, orderdyby, while, while, take
	while, | select                               => while


Each method can have the following

the method, if it is converted into another method, and if it can be followed by another method.
the following is a small convension which these notes are written in.


	method | convert into -> can be followed by (which will be processed as part of the subquery)

======================

P1 is part of a subQuery (Lucene Query)

	where     | * -> *
	order by  | * -> *

======================

P2 modify a part of the lucene query, also signify's the possible end of a subquery

	skip      | *                 -> skip, take, distinct, single, count, any
	take      | *                 -> take, distinct, single, count, any 
	first     | take(1)           -> **acts like a take

======================

P3 is processed on server
(without hitting the another DB. can tell with doc IDs)
these will be process as post luecene subQuery

	distinct  | filter doc ids    ->
	single    | has only 1 ID     ->
	count     | top docs          ->
	any       | has 0 < doc ids   ->


all of the above, we push/hoist the where clause into the subquery where clauses

================================

Sub Query is fully ended at this point the rest of the query is processed "in proc".

	select
	SelectMany
	Aggregate (sum)
	Join
	groupby
	when type changed | -> 
	all

not sure about these nicely

	min
	max 
	last
	contains  | has 0 < doc ids   ->